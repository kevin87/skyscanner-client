# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'skyscanner_client/version'

Gem::Specification.new do |spec|
  spec.name          = "skyscanner_client"
  spec.version       = SkyscannerClient::VERSION
  spec.authors       = ["kevin87"]
  spec.email         = ["kevin.ho@flochip.com"]
  spec.description   = %q{Skyscanner API Client}
  spec.summary       = %q{Skyscanner API Client}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "debugger"
end
