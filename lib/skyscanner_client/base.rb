require 'json'
require 'net/http'

module SkyscannerClient

  class Base
    API_SERVER = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0"

    attr_accessor :options

    def initialize(option = {})
      @api_server = API_SERVER
    end

    def call api_key, params={}
      uri = URI @api_server
      http = Net::HTTP.post_form(uri, params)
      if http.code == "201"
        http.header.each_header do |key,value|
          if key == "location"
            poll_uri = URI(value + "/?apiKey=#{api_key}")
            poll_http = Net::HTTP.get_response(poll_uri)
            response = JSON.parse(poll_http.body)
            return response
          end
        end
      else
        response = JSON.parse(http.body)
        return response
      end
    end

    def book url, params={}
      http = Net::HTTP.new("partners.api.skyscanner.net")
      request = Net::HTTP::Put.new(url)
      request.set_form_data(params)
      response = http.request(request)
      if response.code == "201"
        response.header.each_header do |key,value|
          if key == "location"
            book_uri = URI(value + "/?apiKey=#{params[:apikey]}")
            book_http = Net::HTTP.get_response(book_uri)
            response = JSON.parse(book_http.body)
            return response
          end
        end
      else
        return JSON.parse(response.body)
      end
    end
  end
end
