require 'rubygems'
require "skyscanner_client/version"
require "skyscanner_client/base"

module SkyscannerClient

  module Method
    POST = :post
    GET  = :get
  end

  def create(options = {})
    SkyscannerClient::Base.new(options)
  end

  module_function :create
end
